export const environment = {
    NAME: 'dev',
    PRODUCTION: false,
    IS_TEST_ENV: false,
    BASE_URL_API: 'http://localhost:8081/'
};
