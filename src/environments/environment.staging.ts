export const environment = {
    NAME: 'staging',
    PRODUCTION: false,
    IS_TEST_ENV: true,
    BASE_URL_API: 'http://localhost:8081/'
};
