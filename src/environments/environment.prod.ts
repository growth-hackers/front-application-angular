export const environment = {
    NAME: 'prod',
    PRODUCTION: true,
    IS_TEST_ENV: false,
    BASE_URL_API: 'http://51.38.36.31:8081/'
};
