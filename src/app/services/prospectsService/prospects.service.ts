import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProspectsService {
  baseUrlApi: string;

  constructor(protected http: HttpClient) {
    this.baseUrlApi = `${environment.BASE_URL_API}/`;
  }

  /**
   * Return response of a request who search prospects
   * @param searchId - search id to find prospects
   */
  public getProspects(searchId: string): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${this.baseUrlApi}/prospects/${searchId}`, {observe: 'response'});
  }
}
