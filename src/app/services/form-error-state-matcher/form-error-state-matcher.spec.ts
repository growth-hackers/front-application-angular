import { FormErrorStateMatcher } from './form-error-state-matcher';
import { FormControl, NgForm, Validators } from '@angular/forms';

describe('FormErrorStateMatcher Model Class', () => {
    let formErrorStateMatcher: FormErrorStateMatcher;

    beforeEach(() => {
        formErrorStateMatcher = new FormErrorStateMatcher();
    });

    it('should create a new instance of FormErrorStateMatcher', () => {
        expect(formErrorStateMatcher).toBeInstanceOf(FormErrorStateMatcher);
    });

    describe('isErrorState Method', () => {
        let form: NgForm,
            formControl: FormControl;

        it('should return false if control is null', () => {
            expect(formErrorStateMatcher.isErrorState(null, null)).toBeFalsy();
        });

        it('should return false if control is invalid but not dirty or touched or submitted', () => {
            formControl = new FormControl('', Validators.required);
            expect(formErrorStateMatcher.isErrorState(formControl, null)).toBeFalsy();
        });

        it('should return false if control is not invalid but dirty', () => {
            formControl = new FormControl('value');
            formControl.markAsDirty();
            expect(formErrorStateMatcher.isErrorState(formControl, null)).toBeFalsy();
        });

        it('should return false if control is not invalid but touched', () => {
            formControl = new FormControl('value');
            formControl.markAsTouched();
            expect(formErrorStateMatcher.isErrorState(formControl, null)).toBeFalsy();
        });

        it('should return true if control is invalid and dirty', () => {
            formControl = new FormControl('', Validators.required);
            formControl.markAsDirty();
            expect(formErrorStateMatcher.isErrorState(formControl, null)).toBeTruthy();
        });

        it('should return true if control is invalid and touched', () => {
            formControl = new FormControl('', Validators.required);
            formControl.markAsTouched();
            expect(formErrorStateMatcher.isErrorState(formControl, null)).toBeTruthy();
        });

        it('should return true if control is invalid and form is submitted', () => {
            formControl = new FormControl('', Validators.required);
            const form = <NgForm>{
                value: {
                    name: 'value',
                },
                submitted: true
            };
            expect(formErrorStateMatcher.isErrorState(formControl, form)).toBeTruthy();
        });

        afterEach(() => {
            formControl = null;
            form = null;
        });
    });
});
