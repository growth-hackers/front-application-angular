import { getTestBed, TestBed } from '@angular/core/testing';
import { HandlingStatusCodeService } from './handling-status-code.service';

describe('HandlingStatusCodeService', function () {
    let injector: TestBed,
        handlingStatusCodeService: HandlingStatusCodeService,
        response: object;

    beforeEach(function () {
        TestBed.configureTestingModule({
            providers: [HandlingStatusCodeService]
        });
        injector = getTestBed();
        handlingStatusCodeService = injector.inject(HandlingStatusCodeService);
    });

    it('should create a service: handlingStatusCodeService', function () {
        expect(handlingStatusCodeService).toBeTruthy();
    });

    describe('getValueInFunctionOfStatusCode Method', function () {

        it('should return result with status code 200', function () {
            response = {
                status: 200,
                body: {success: true, result: [{id: 1, name: 'name'}]}
            };

            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => {
                    expect(Array.isArray(responseResult)).toBeTruthy();
                    expect(responseResult).toEqual(response['body'].result);
                });
        });

        it('should return message with status code 200', function () {
            response = {
                status: 200,
                body: {success: true, message: 'Everything is ok'}
            };

            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => expect(responseResult).toEqual(response['body'].message));
        });

        it('should return result with status code 201', function () {
            response = {
                status: 201,
                body: {success: true, result: [{id: 1, name: 'name'}]}
            };

            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => {
                    expect(Array.isArray(responseResult)).toBeTruthy();
                    expect(responseResult).toEqual(response['body'].result);
                });
        });

        it('should return message with status code 201', function () {
            response = {
                status: 201,
                body: {success: true, message: 'New object created'}
            };

            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => expect(responseResult).toEqual(response['body'].message));
        });

        it('should return text with no result with status code 204', function () {
            response = {status: 204};

            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => expect(responseResult).toEqual('Aucun résultat trouvé'));
        });

        it('should return null by default', function () {
            response = {status: 500};
            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .then(responseResult => expect(responseResult).toBeNull());
        });

        it('should reject an error if no status in response is found', function () {
            response = {};
            return handlingStatusCodeService.getValueInFunctionOfStatusCode(response)
                .catch(error => expect(error.message).toEqual('No status response found'));
        });
    });
});
