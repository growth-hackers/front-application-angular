import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HandlingStatusCodeService {
    dataResponse: any;

    constructor() { }

    /**
     * Get value in function of status code returned by http's response
     * @param response returned by a http's request
     */
    getValueInFunctionOfStatusCode(response): any {
        return new Promise((resolve, reject) => {
            if (!response.status) {
                reject(new Error('No status response found'));
            } else {
                switch (response.status) {
                    case 200:
                        this.dataResponse = response.body;
                        break;
                    case 201:
                        this.dataResponse = response.body;
                        break;
                    case 204:
                        this.dataResponse = 'Aucun résultat trouvé';
                        break;
                    default:
                        this.dataResponse = null;
                        break;
                }
                resolve(this.dataResponse);
            }
        });
    }
}
