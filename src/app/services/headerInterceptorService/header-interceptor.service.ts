import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from '../authService/auth.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class HeaderInterceptorService implements HttpInterceptor {

    constructor(private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authService.token != null) {
            request = request.clone({
                setHeaders: { 'Authorization': this.authService.token }
            });
        }
        return next.handle(request);
    }
}
