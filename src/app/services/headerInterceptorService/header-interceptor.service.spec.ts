import { getTestBed, inject, TestBed } from '@angular/core/testing';
import { HeaderInterceptorService } from './header-interceptor.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('HeaderInterceptorService', function () {
    let injector: TestBed,
        headerInterceptorService: HeaderInterceptorService,
        httpMock: HttpTestingController;

    beforeEach(function () {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HeaderInterceptorService,
                    multi: true
                }
            ]
        });
        injector = getTestBed();
        headerInterceptorService = TestBed.inject(HeaderInterceptorService);
        httpMock = injector.inject(HttpTestingController);
    });

    it('should create a service: headerInterceptorService', function () {
        expect(headerInterceptorService).toBeTruthy();
    });

    it('should not add token in header', inject([HttpClient], function (http: HttpClient) {
        const getTokenSpy = jest.spyOn(headerInterceptorService['authService'], 'token', 'get').mockReturnValue(null);
        http.get('/fake').subscribe();
        const httpRequest = httpMock.expectOne('/fake');
        expect(httpRequest.request.headers.has('Authorization')).toBeFalsy();
        expect(getTokenSpy).toHaveBeenCalledTimes(1);
    }));

    it('should add token in header', inject([HttpClient], function (http: HttpClient) {
        const getTokenSpy = jest.spyOn(headerInterceptorService['authService'], 'token', 'get').mockReturnValue('token');
        http.get('/fake').subscribe();
        const httpRequest = httpMock.expectOne('/fake');
        expect(httpRequest.request.headers.has('Authorization')).toBeTruthy();
        expect(getTokenSpy).toHaveBeenCalledTimes(2);
    }));

    afterEach(function() {
        httpMock.verify();
    });
});
