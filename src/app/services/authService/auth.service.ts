import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment';
import {CookieService} from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrlApi: string;

  constructor(protected http: HttpClient,
              private cookieService: CookieService) {
    this.baseUrlApi = `${environment.BASE_URL_API}/`;
  }

  /**
   * Get token with cookie
   */
  get token() {
    const token = this.cookieService.get('token');
    if (token !== '') {
      return token;
    }
  }

  /**
   * Save token in cookie
   * @param token - token took with login request
   */
  public saveToken(token: string) {
    return this.cookieService.set('token', token);
  }

  /**
   * destroy token in cookie
   */
  public destroyToken() {
    if (this.cookieService.get('token') !== '') {
      return this.cookieService.delete('token');
    }
  }

  /**
   * Return response of a request who create new user
   */
  public createUser(user: object): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.baseUrlApi}/create_user`, user, {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), observe: 'response'});
  }

  /**
   * Request to log and return response to create token
   * @param userMailAndPasswordObj - object with mail and password
   */
  public login(userMailAndPasswordObj): Observable<any> {
    return this.http.post<any>(`${this.baseUrlApi}/new_token`, userMailAndPasswordObj,
      {headers: new HttpHeaders({ 'Content-Type': 'application/json' }), observe: 'response'});
  }
}
