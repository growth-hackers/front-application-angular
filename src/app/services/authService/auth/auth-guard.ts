import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router,
                private cookieService: CookieService) {}

    /**
     * To check authorization to access to some page
     */
    canActivate() {
      if (this.cookieService.get('token') !== '') {
        return true;
      } else {
        return this.router.navigate(['/register']);
      }
    }
}
