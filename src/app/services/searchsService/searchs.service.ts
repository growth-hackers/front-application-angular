import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SearchsService {
  baseUrlApi: string;

  constructor(protected http: HttpClient) {
    this.baseUrlApi = `${environment.BASE_URL_API}/`;
  }

  /**
   * Return response of a request to search in history
   */
  public getHistorySearch(): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${this.baseUrlApi}/searchs_history`, {observe: 'response'});
  }
}
