import { Component, Input, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/authService/auth.service';
import {HandlingStatusCodeService} from '../../services/statusCodeService/handling-status-code.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  authForm: FormGroup;

    constructor(private formBuilder: FormBuilder,
                private authService: AuthService,
                private router: Router,
                private handlingStatusCodeService: HandlingStatusCodeService) {
    }

    ngOnInit(): void {
      this.authForm = this.formBuilder.group({
        email: [null, [
          Validators.required,
          Validators.email
        ]],
        password: [null, Validators.required]
      });
    }

  /**
   * Get email for FormGroup
   */
  get email() {
    return this.authForm.get('email');
  }

  /**
   * Get password for FormGroup
   */
  get password() {
    return this.authForm.get('password');
  }

  /**
   * Get some action when user click on button submit
   * @param dataValue - form's data value
   */
  onSubmit(dataValue: object) {
    if (this.authForm.valid) {
      let emailAndPasswordObj: object;

      emailAndPasswordObj = {mail: dataValue['email'].toLowerCase(), password: dataValue['password']};
      return this.authService.login(emailAndPasswordObj).toPromise().then(res => {
        return this.handlingStatusCodeService.getValueInFunctionOfStatusCode(res)
          .then(responseToken => {
            console.log(responseToken);
            //todo: to know what response is given by api ?
            //todo: handle and show error if email or password are wrong
            return this.authService.saveToken(responseToken);
          }).then(() => {
            return this.router.navigate(['/dashboard']);
          });
      });
    }
  }
}
