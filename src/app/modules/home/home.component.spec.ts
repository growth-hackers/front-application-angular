import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { homeRoutes } from './home-routing.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('HomeComponent', function () {
    let component: HomeComponent,
        fixture: ComponentFixture<HomeComponent>;

    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [HomeComponent],
            imports: [
                RouterTestingModule.withRoutes(homeRoutes),
                NoopAnimationsModule
            ]
        }).compileComponents();
    }));

    beforeEach(function () {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
    });

    it('should create home component', function () {
        expect(component).toBeTruthy();
    });
});
