import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { dashboardRoutes } from './dashboard-routing.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('DashboardComponent', function () {
    let component: DashboardComponent,
        fixture: ComponentFixture<DashboardComponent>;

    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DashboardComponent],
            imports: [
                RouterTestingModule.withRoutes(dashboardRoutes),
                NoopAnimationsModule
            ]
        }).compileComponents();
    }));

    beforeEach(function () {
        fixture = TestBed.createComponent(DashboardComponent);
        component = fixture.componentInstance;
    });

    it('should create dashboard component', function () {
        expect(component).toBeTruthy();
    });
});
