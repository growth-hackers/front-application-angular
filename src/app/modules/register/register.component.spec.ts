import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { RouterTestingModule } from '@angular/router/testing';
import { registerRoutes } from './register-routing.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('RegisterComponent', function () {
    let component: RegisterComponent,
        fixture: ComponentFixture<RegisterComponent>;

    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [RegisterComponent],
            imports: [
                RouterTestingModule.withRoutes(registerRoutes),
                NoopAnimationsModule
            ]
        }).compileComponents();
    }));

    beforeEach(function () {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
    });

    it('should create register component', function () {
        expect(component).toBeTruthy();
    });
});
