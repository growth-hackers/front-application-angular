import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormErrorStateMatcher} from '../../services/form-error-state-matcher/form-error-state-matcher';

@NgModule({
    declarations: [RegisterComponent],
    imports: [CommonModule,
      RegisterRoutingModule,
      MatButtonModule,
      ReactiveFormsModule,
      MatFormFieldModule,
      MatInputModule
    ],
  providers: [
    { provide: ErrorStateMatcher, useClass: FormErrorStateMatcher }
  ]
})
export class RegisterModule {}
