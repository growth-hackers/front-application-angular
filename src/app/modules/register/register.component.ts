import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {AuthService} from '../../services/authService/auth.service';
import {Router} from '@angular/router';
import {HandlingStatusCodeService} from '../../services/statusCodeService/handling-status-code.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

    constructor(private formBuilder: FormBuilder,
                private authService: AuthService,
                private router: Router,
                private handlingStatusCodeService: HandlingStatusCodeService) {}

    ngOnInit(): void {
      this.registerForm = this.formBuilder.group({
        name: [null, [
          Validators.required,
          Validators.pattern(/^[a-zA-Z\u00C0-\u00FF'’.,\-\s]*$/)
        ]],
        surname: [null, [
          Validators.required,
          Validators.pattern(/^[a-zA-Z\u00C0-\u00FF'’.,\-\s]*$/)
        ]],
        phone: [null, [
          Validators.required,
          Validators.pattern(/(\+)?[0-9]/)
        ]],
        email: [null, [
          Validators.required,
          Validators.email
        ]],
        password: [null, [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20)
        ]],
        confirmpwd: [null, [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          this.checkConfirmPassword
        ]]
      });
    }

  /**
   * Get name for FormGroup
   */
  get name() {
    return this.registerForm.get('name');
  }

  /**
   * Get surname for FormGroup
   */
  get surname() {
    return this.registerForm.get('surname');
  }

  /**
   * Get phone for FormGroup
   */
  get phone() {
    return this.registerForm.get('phone');
  }

  /**
   * Get email for FormGroup
   */
  get email() {
    return this.registerForm.get('email');
  }

  /**
   * Get password for FormGroup
   */
  get password() {
    return this.registerForm.get('password');
  }

  /**
   * Get confirm password for FormGroup
   */
  get confirmpwd() {
    return this.registerForm.get('confirmpwd');
  }

  /**
   * Get some action when user click on button submit
   * @param dataValue - form's data value
   */
  onSubmit(dataValue: object) {
    if (this.registerForm.valid) {

      const user = {
        mail: dataValue['mail'],
        password: dataValue['password'],
        firstname: dataValue['name'],
        lastname: dataValue['surname'],
        phone: dataValue['phone']
      };

      const userMailAndPassword = {
        mail: dataValue['mail'],
        password: dataValue['password']
      };

      return this.authService.createUser(user).toPromise().then(res => {
        return this.handlingStatusCodeService.getValueInFunctionOfStatusCode(res);
      }).then(() => {
        return this.authService.login(userMailAndPassword).toPromise().then(res => {
          return this.handlingStatusCodeService.getValueInFunctionOfStatusCode(res);
        }).then(responseToken => {
          console.log(responseToken);
          //todo: to know what response is given by api ?
          return this.authService.saveToken(responseToken);
        }).then(() => {
          return this.router.navigate(['/dashboard']);
        });
      });
    }
  }

  /**
   * Validator to limit max length of file input to send
   */
  checkConfirmPassword(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let isSamePassword: boolean;
      if (control && control.value) {
        isSamePassword = control.value === this.registerForm.get('password');
      } else {
        isSamePassword = false;
      }

      return isSamePassword ? null : {confirmPassword: 'password and confirm password are not same'};
    };
  }
}
