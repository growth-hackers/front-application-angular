import {browser, by, element} from 'protractor';

export class AppPage {

  /////////////////////////
  // NAVIGATIONS HELPERS
  ////////////////////////

  /**
   * Navigate to a path
   * @param path to go for navigation
   */
  navigateTo(path: string) {
    return browser.get(path);
  }

  /**
   * Get current Url
   */
  getCurrentUrl() {
    return browser.getCurrentUrl()
      .then(currentUrl => {
        return currentUrl.replace(browser.baseUrl, '/');
      });
  }

  /**
   * Know if url changed or not
   * @param url before action
   */
  urlChanged(url: string) {
    return browser.getCurrentUrl()
      .then(actualUrl => {
        const actualUrlWithoutBaseUrl = actualUrl.replace(browser.baseUrl, '/');
        return url !== actualUrlWithoutBaseUrl;
      });
  }


  /////////////////////////
  // ELEMENTS HELPERS
  ////////////////////////

  /**
   * Get an element (button, link, div etc..)
   * @param name id/class or other to identify element
   */
  getElement(name: string) {
    return element(by.css(name));
  }

  /**
   * Get an array element
   * @param name id/class or other to identify element
   */
  getArrayElement(name: string) {
    return element.all(by.css(name));
  }

  /**
   * Scroll to an element
   * @param name id to identify element
   * @param index element's number to scroll
   */
  scrollToElement(name: string, index: number) {
    const elementToScroll = element.all(by.css(name)).get(index);
    return browser.driver.executeScript('arguments[0].scrollIntoView();', elementToScroll.getWebElement());
  }



  /////////////////////////
  // DESKTOP/MOBILE HELPERS
  ////////////////////////

  /**
   * Update width and height for testing mobile version
   */
  getMobileVersion() {
    const width = 375,
      height = 667;
    return browser.driver.manage().window().setSize(width, height);
  }

  /**
   * Update width and height for testing desktop version
   */
  getDesktopVersion() {
    const width = 1200,
      height = 850;
    return browser.driver.manage().window().setSize(width, height);
  }

  /////////////////////////
  // OTHER HELPERS
  ////////////////////////

  /**
   * Delete a cookie
   * @param name - cookie name to delete
   */
  deleteCookie(name: string) {
    return browser.driver.manage().deleteCookie(name);
  }
}
