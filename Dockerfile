# NODE ANGULAR
FROM node:12

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY ./ /usr/src/app

EXPOSE 4200

RUN npm ci && npm run build:ssr && npm run serve:ssr
